import './card.css'

function Card(props){
return(
   <div className='card'>
       <div className='card__img'>
           <img src={props.thumb}/>
       </div>
       <div className='content'>
            <h2>{props.title}</h2>
           <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, similique reiciendis? Omnis deserunt</p>
           <a href={props.url}  target="_blank">Imagem Completa</a>
       </div>
   </div>
)
}

export default Card