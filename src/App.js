import { useEffect, useState } from "react";
import "./App.css";
import Card from "./components/card.jsx";

function App() {
  const [infos, setInfos] = useState([]);

  useEffect(() => {
    getDados();
  });

  async function getDados() {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/photos"
    ).then((response) => response.json());
    setInfos(response);
  }
  return (
    <div className="App">
      {infos.map((info) => {
        return (
          <Card key={info.id} title={info.title} thumb={info.thumbnailUrl} url={info.url} />
        );
      })}
    </div>
  );
}

export default App;
